/*
 * @Author: xfworld
 * @Date: 2020-03-13 16:47:11
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-10 11:07:02
 * @Description: 
 */
// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  "plugins": {
    // to edit target browsers: use "browserslist" field in package.json
    "autoprefixer": {
      overrideBrowserslist: ["Android >= 4.0", "iOS >= 7"]
    },
    "postcss-pxtorem": {
        rootValue: 37.5,
        propList: ["*"]
    }
  }
}
