/*
 * @Author: xfworld
 * @Date: 2020-03-13 15:56:17
 * @LastEditors: xfworld
 * @LastEditTime: 2020-03-13 16:28:55
 * @Description:
 */
module.exports = {
  root: true,
  env: {
    node: true
  },
  plugins: ["vue"],
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    //生成器函数*的前后空格
    "generator-star-spacing": "off",
    "vue/no-parsing-error": [
      2,
      {
        "x-invalid-end-tag": false
      }
    ],
    //不能有未定义的变量
    "no-undef": "off",
    //强制驼峰法命名
    "camelcase": "off",
    //禁止使用console
    "no-console": "off",
    //箭头函数用小括号括起来
    "arrow-parens": 0
  }
};
