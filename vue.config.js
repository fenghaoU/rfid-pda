/*
 * @Author: xfworld
 * @Date: 2019-08-09 09:45:14
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-05 17:53:57
 * @Description:
 */
const path = require("path");

const resolve = dir => {
  return path.join(__dirname, dir);
};

// 项目部署基础
// 默认情况下，我们假设你的应用将被部署在域的根目录下,
// 例如：https://www.my-app.com/
// 默认：'/'
// 如果您的应用程序部署在子路径中，则需要在这指定子路径
// 例如：https://www.foobar.com/my-app/
// 需要将它改为'/my-app/'
const BASE_URL = "./";
const debug = process.env.NODE_ENV !== "production";

module.exports = {
  // Project deployment base
  // By default we assume your app will be deployed at the root of a domain,
  // e.g. https://www.my-app.com/
  // If your app is deployed at a sub-path, you will need to specify that
  // sub-path here. For example, if your app is deployed at
  // https://www.foobar.com/my-app/
  // then change this to '/my-app/'
  publicPath: BASE_URL,
  /* 输出文件目录：在npm run build时，生成文件的目录名称 */
  outputDir: "dist",
  /* 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录 */
  assetsDir: "static",
  /* 默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存，你可以通过将这个选项设为 false 来关闭文件名哈希。(false的时候就是让原来的文件名不改变) */
  filenameHashing: true,
  // assetsDir: './',
  // tweak internal webpack configuration.
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  // 如果你不需要使用eslint，把lintOnSave设为false即可
  lintOnSave: true,
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          "green": "#ed6d00",
          "tabbar-item-text-color": "#9B9B9B",
          "tabbar-item-active-color": "#ed6d00",
          "tabbar-item-font-size": "13px",
          "nav-bar-icon-color": "#ed6d00",
          "nav-bar-text-color": "#ed6d00",
          "dialog-confirm-button-text-color": "#ed6d00"
        }
      }
    }
  },
  configureWebpack: config => {
    if (debug) {
      // 开发环境配置
      // 1、source-map：产生文件，产生行列
      // 2、eval-source-map：不产生文件，产生行类
      // 3、cheap-source-map：产生文件，不产生列
      // 4、cheap-module-eval-source-map：不产生文件，不产生列
      // 开发环境：cheap-module-eval-source-map   生产环境： cheap - module - source - map
      config.devtool = "source-map";
    }
    // 配置打包 js、css文件为.gz格式，优化加载速度  （参考：https://blog.csdn.net/qq_31677507/article/details/102742196）
    // if (process.env.NODE_ENV === "production") {
    //     return {
    //         plugins: [
    //             new CompressionPlugin({
    //                 test: /\.js$|\.css/, // 匹配文件
    //                 threshold: 10240, // 超过10kB的数据进行压缩
    //                 deleteOriginalAssets: false // 是否删除原文件 （原文件也建议发布到服务器以支持不兼容gzip的浏览器）
    //             })
    //         ],
    //         performance: {
    //             // 生产环境构建代码文件超出以下配置大小会在命令行中显示警告
    //             hints: "warning",
    //             // 入口起点的最大体积 整数类型（以字节为单位,默认值是：250000 (bytes)）
    //             maxEntrypointSize: 5000000,
    //             // 生成文件的最大体积 整数类型（以字节为单位,默认值是：250000 (bytes)）
    //             maxAssetSize: 3000000
    //             // // 只给出 js 文件的性能提示
    //             // assetFilter: function (assetFilename) {
    //             //   return assetFilename.endsWith('.js')
    //             // }
    //         }
    //     };
    // }
  },
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");
    // 清除已有的所有 loader。
    // 如果你不这样做，接下来的 loader 会附加在该规则现有的 loader 之后。
    svgRule.uses.clear();
    svgRule
      .test(/\.svg$/)
      .include.add(path.resolve(__dirname, "./src/icons/svg"))
      .end()
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .options({
        symbolId: "icon-[name]"
      });
    const fileRule = config.module.rule("file");
    fileRule.uses.clear();
    fileRule
      .test(/\.svg$/)
      .exclude.add(path.resolve(__dirname, "./src/icons/svg"))
      .end()
      .use("file-loader")
      .loader("file-loader");
    // 移除 prefetch 插件(针对生产环境首屏请求数进行优化)
    //config.plugins.delete("prefetch");
    // 移除 preload 插件(针对生产环境首屏请求数进行优化)   preload 插件的用途：https://cli.vuejs.org/zh/guide/html-and-static-assets.html#preload
    //config.plugins.delete("preload");
    config.resolve.alias
      .set("@", resolve("src")) // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set("_c", resolve("src/components"))
      .set("@views", resolve("src/views"))
      .set("@router", resolve("src/router"))
      .set("@store", resolve("src/store"))
      .set("@libs", resolve("src/libs"))
      .set("@utils", resolve("src/utils"));
  },
  // 是否为 Babel 或 TypeScript 使用 threa-loader。
  // 该选项在系统的 CPU 有多于一个内核时自动启用，仅作用于生产构建
  parallel: require("os").cpus().length > 1,
  // 设为false打包时不生成.map文件
  productionSourceMap: true
  // 这里写你调用接口的基础路径，来解决跨域，如果设置了代理，那你本地开发环境的axios的baseUrl要写为 '' ，即空字符串
  // devServer: {
  //   proxy: 'localhost:3000'
  // }
};
