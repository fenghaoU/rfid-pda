/*
 * @Author: xfworld
 * @Date: 2020-03-13 15:56:17
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-09 16:55:45
 * @Description: fdsf
 */
import Vue from "vue";
import router from "./router";
import store from "./store";
import App from "./App.vue";
import "amfe-flexible";
import Vant from "vant";
import "vant/lib/index.less";
//  引用本地字库
import "vant/lib/icon/local.less";
import config from "@/config";
import "@/icons";
import "@/assets/css/common.less";

Vue.use(Vant);

//将全局变量模块挂载到Vue.prototype中
Vue.prototype.$config = config;

Vue.config.productionTip = true;
Vue.config.devtools = true;

new Vue({
  router,
  store,
  render: h => h(App),
  data: function() {
    return {
      storerWarehouseBrand: {}
    };
  }
}).$mount("#app");
