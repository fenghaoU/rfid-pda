/*
 * @Author: xfworld
 * @Date: 2020-06-08 16:34:06
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-09 10:30:28
 * @Description:
 */

export default {
  props: {
    menuItem: {
      type: Object,
      default: () => {}
    }
  },
  computed: {
    menuNum() {
      return this.menuItem.num;
    },
    children() {
      return this.menuItem.children;
    },
    iconSize() {
      if (this.menuItem.iconSize !== null && this.menuItem.iconSize !== undefined) {
        return this.menuItem.iconSize + "rem";
      } else {
        return "0.6rem";
      }
    }
  }
};
