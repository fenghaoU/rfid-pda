/*
 * @Author: xfworld
 * @Date: 2020-06-08 16:34:06
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-10 16:07:05
 * @Description:
 */

export default {
  props: {
    topItem: {
      type: Object,
      default: () => {}
    }
  },
  computed: {
    topTitle() {
      return this.topItem.title;
    },
    topGoBackUrl() {
      return this.topItem.url;
    }
  }
};
