/*
 * @Author: xfworld
 * @Date: 2020-06-08 16:34:06
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 16:06:32
 * @Description:
 */

export default {
  props: {
    footerItem: {
      type: Object,
      default: () => {}
    }
  },
  computed: {
    footerTabList() {
      return this.footerItem.tabList;
    },
    footerActive() {
      return this.footerItem.active;
    },
    footerShow() {
      const {path} = this.$route;
      if (this.footerItem.tabList !== null && this.footerItem.tabList !== undefined) {
        return this.footerItem.tabList.some(item => item.path === path);
      }
    }
  }
};
