/*
 * @Author: xfworld
 * @Date: 2020-04-22 11:23:51
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 11:00:47
 * @Description:
 */

const state = {
  tabList: [
    {
      title: "首页",
      path: "/home/index",
      icon: "wap-home"
    },
    /*  {
            title: "B2B",
            path: "/home/b2b",
            icon: "shop"
        },
        {
            title: "B2C",
            path: "/home/b2c",
            icon: "shopping-cart"
        },*/
    {
      title: "我的",
      path: "/home/person",
      icon: "manager"
    }
    // {
    //     title: "更多",
    //     path: "/more",
    //     icon: "setting"
    // }
  ]
};

const mutations = {};

const actions = {};

const getters = {
  mainBarList: state => state.tabList
};

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions
};
