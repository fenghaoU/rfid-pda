/*
 * @Author: xfworld
 * @Date: 2020-05-26 11:37:57
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-10 12:09:24
 * @Description:
 */
import config from "@/config";
import {login} from "@/api/login";
import {getUserView} from "@/api/system/alternatelyUser";
import {queryAdminRole} from "@/api/system/userRole";
import {setToken, getToken, localSave, localRead} from "@/libs/util";

const {appNameSpace} = config;

const state = {
  userName: localRead(appNameSpace + "userName") || "",
  userId: localRead(appNameSpace + "userId") || "",
  token: getToken(),
  userData: localRead(appNameSpace + "userData") ? JSON.parse(localRead(appNameSpace + "userData")) : {},
  access: localRead(appNameSpace + "userAccess") ? JSON.parse(localRead(appNameSpace + "userAccess")) : [],
  adminStatus: localRead(appNameSpace + "adminStatus") || false,
  hasGetInfo: false
};

const mutations = {
  setUserId(state, id) {
    state.userId = id;
  },
  setUserData(state, data) {
    state.userData = data;
  },
  setUserName(state, name) {
    state.userName = name;
  },
  setAccess(state, access) {
    state.access = access;
  },
  setAdminStatus(state, status) {
    state.adminStatus = status;
  },
  setToken(state, token) {
    state.token = token;
    setToken(token);
  },
  setHasGetInfo(state, status) {
    state.hasGetInfo = status;
  }
};

const actions = {
  // 设定用户登录信息
  setUserInfo({dispatch, commit}, res) {
    localSave(appNameSpace + "userId", res.id);
    commit("setToken", res.token);
    commit("setUserId", res.id);
    dispatch("getUserInfo");
    dispatch("getUserAccess");
  },
  /**
   * 退出重置用户登录信息
   */
  setSignOut({commit}) {
    localSave(appNameSpace + "userId", "");
    localSave(appNameSpace + "userName", "");
    localSave(appNameSpace + "userData", "");
    localSave(appNameSpace + "userAccess", "");
    localSave(appNameSpace + "adminStatus", "");

    commit("setUserId", "");
    commit("setUserName", "");
    commit("setUserData", "");
    commit("setAccess", []);
    commit("setToken", "");
    commit("setAdminStatus", false);
    commit("setHasGetInfo", false);
  },
  // 登录
  handleLogin({dispatch, commit}, {userName, password}) {
    userName = userName.trim();
    return new Promise((resolve, reject) => {
      login({userName, password})
        .then(res => {
          if (res !== null && res.data !== null) {
            if (res.data.status) {
              commit("setUserName", userName);
              localSave(appNameSpace + "userName", userName);
              dispatch("setUserInfo", res.data.data);
            }
          }
          // const data = res.data
          // commit('setToken', data.token)
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  // 退出登录
  handleLogOut({dispatch}) {
    return new Promise(resolve => {
      dispatch("setSignOut");
      resolve();
    });
  },
  // 获取用户相关信息
  getUserInfo({state, commit}) {
    return new Promise((resolve, reject) => {
      try {
        getUserView(state.userId)
          .then(res => {
            const data = res.data.data;
            localSave(appNameSpace + "userData", JSON.stringify(data));
            commit("setUserData", data);
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      } catch (error) {
        reject(error);
      }
    });
  },
  // 获取用户的授权菜单项
  getUserAccess({dispatch, state, commit}) {

  },
  //获取管理信息
  currentUserAdminStatus({state, commit}) {
    return new Promise((resolve, reject) => {
      if (state.userData.sysUserType === 1) {
        localSave(appNameSpace + "adminStatus", true);
        commit("setAdminStatus", true);
      } else {
        queryAdminRole(state.userId)
          .then(res => {
            if (res !== null && res.data !== null && res.data.status) {
              if (res.data.data) {
                localSave(appNameSpace + "adminStatus", true);
                commit("setAdminStatus", true);
              }
            }
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      }
    });
  }
};

const getters = {
  token: state => state.token,
  userId: state => state.userId,
  userData: state => state.userData,
  userAccess: state => state.access,
  adminStatus: state => state.adminStatus
};

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions
};
