/*
 * @Author: xfworld
 * @Date: 2020-05-26 11:38:05
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 17:04:29
 * @Description:
 */

import Scanner from "@utils/scanner";

const state = {
  systemScannerEnable: "",
  systemScanner: "",
  systemScannerResult: {data: "", time: ""},
  systemScannerError: ""
};

const mutations = {
  setScanner(state, pdaConfig) {
    state.systemScannerEnable = pdaConfig.use;
    state.systemScanner = new Scanner(pdaConfig);
  },
  setScanData(state, data) {
    state.systemScannerResult.data = data;
    state.systemScannerResult.time = new Date().getTime();
  },
  setScanFail(state, error) {
    state.systemScannerError = error;
  }
};

const actions = {
  initScanEvent({commit}, pdaConfig) {
    commit("setScanner", pdaConfig);
  },
  scanSuccessEvent({dispatch, commit}, scanData) {
    return new Promise(resolve => {
      commit("setScanData", scanData);
      dispatch("scanNoticeEvent");
      resolve();
    });
  },
  scanFailEvent({dispatch, commit}, scanError) {
    return new Promise(resolve => {
      commit("setScanFail", scanError);
      dispatch("scanNoticeEvent");
      resolve();
    });
  },
  scanNoticeEvent({state}) {
    console.log("stateResult  -->" + JSON.stringify(state.systemScannerResult));
  }
};

const getters = {
  pdaUseEnable: state => state.systemScannerEnable,
  pdaScanner: state => state.systemScanner,
  pdaScanData: state => state.systemScannerResult,
  pdaScanError: state => state.systemScannerError
};

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions
};
