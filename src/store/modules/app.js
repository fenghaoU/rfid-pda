/*
 * @Author: xfworld
 * @Date: 2020-05-26 11:37:52
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-05 17:12:11
 * @Description:
 */

const state = {
  systemLoading: true,
  pickScanInfo: {
    pickTaskCode: "",
    cartonNo: "",
    locationNoCodeNum: ""
  }
};

const mutations = {
  setSystemLoading(state, status) {
    state.systemLoading = status;
  },
  setPickInfo(state, data) {
    state.pickScanInfo = data;
  }
};

const actions = {
  setLoadingStatus({commit}, status) {
    commit("setSystemLoading", status);
  }
};

const getters = {
  loading: state => state.systemLoading,
  getPickScanInfo: state => state.pickScanInfo
};

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions
};
