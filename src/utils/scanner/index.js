/*
 * @Author: xfworld
 * @Date: 2020-05-13 15:49:23
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-02 11:48:14
 * @Description:
 */
export default class Scanner {
  constructor(scannerConfig) {
    this.use = scannerConfig.use;
    this.type = scannerConfig.type;
  }
  nativeListen(plugin, dataFunction, errorFunction) {
    if (this.use) {
      if (plugin != null && plugin != undefined) {
        if (this.type === "zebra_rfid") {
          if (plugin.zebra_rfid != null && plugin.zebra_rfid != undefined) {
            plugin.zebra_rfid.nativeListen(
              data => {
                dataFunction(data);
              },
              error => {
                errorFunction(error);
              }
            );
          }
        }
      }
    }
  };
  scanSettings(plugin, dataFunction, errorFunction,params) {
    if (this.use) {
      if (plugin != null && plugin != undefined) {
        if (this.type === "zebra_rfid") {
          if (plugin.zebra_rfid != null && plugin.zebra_rfid != undefined) {
            plugin.zebra_rfid.scanSettings(
              data => {
                dataFunction(data);
              },
              error => {
                errorFunction(error);
              },
              // 扫描设置参数
              params
            );
          }
        }
      }
    }
  };
}
