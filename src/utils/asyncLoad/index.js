/*
 * @Author: xfworld
 * @Date: 2020-06-04 13:13:21
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-10 16:16:18
 * @Description:
 */
import Loading from "@/components/main/loading";
import store from "@/store";
// 异步组件
const asyncComponent = asyncFun => {
  store.dispatch("app/setLoadingStatus", true);
  const component = () => ({
    component: asyncFun(),
    loading: Loading
  });
  return {
    //   必须提供一个render 函数
    render(h) {
      return h(component);
    }
  };
};

export default asyncComponent;
