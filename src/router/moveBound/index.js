/*
 * @Author: xfworld
 * @Date: 2020-06-11 11:31:36
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 11:34:09
 * @Description:
 */
const moveBound = [
  {
    path: "/moveBound",
    name: "moveBound",
    component: () => import("@/components/main/homeView"),
    children: [
      {
        path: "index",
        name: "moveBoundIndex",
        component: () => import("@/views/biz/moveBound/index")
      },
      {
        path: "quality",
        name: "qualityIndex",
        component: () => import("@/views/biz/moveBound/quality/index")
      },
      {
        path: "quality/picking",
        name: "qualityPicking",
        component: () => import("@/views/biz/moveBound/quality/picking.vue")
      },
      {
        path: "quality/offShelf",
        name: "qualityOffShelf",
        component: () => import("@/views/biz/moveBound/quality/offShelf.vue")
      },
      {
        path: "quality/onShelf",
        name: "qualityOnShelf",
        component: () => import("@/views/biz/moveBound/quality/onShelf.vue")
      },
      {
        path: "quality/onShelfBycase",
        name: "qualityOnShelfBycase",
        component: () => import("@/views/biz/moveBound/quality/onShelfBycase.vue")
      },
      {
        path: "move",
        name: "moveIndex",
        component: () => import("@/views/biz/moveBound/move/index")
      },
      {
        path: "returnWarehouse",
        name: "returnWarehouseIndex",
        component: () => import("@/views/biz/moveBound/returnWarehouse/index")
      },
      {
        path: "moveTakeout",
        name: "moveTakeout",
        component: () => import("@/views/biz/moveBound/move/moveTakeout"),
        meta: {title: "移库下架"}
      },
      {
        path: "moveBoxPutaway",
        name: "moveBoxPutaway",
        component: () => import("@/views/biz/moveBound/move/moveBoxPutaway"),
        meta: {title: "按箱上架"}
      },
      {
        path: "moveItemPutaway",
        name: "moveItemPutaway",
        component: () => import("@/views/biz/moveBound/move/moveItemPutaway"),
        meta: {title: "按件上架"}
      },
      {
        path: "return",
        name: "return",
        component: () => import("@/views/biz/moveBound/returnWarehouse/return"),
        meta: {
          title: "还货"
        }
      },
      {
        path: "returnScan",
        name: "returnScan",
        component: () => import("@/views/biz/moveBound/returnWarehouse/returnScan"),
        meta: {title: "二维码还货"}
      },
      {
        path: "barCodeReturnScan",
        name: "barCodeReturnScan",
        component: () => import("@/views/biz/moveBound/returnWarehouse/barCodeReturnScan"),
        meta: {title: "条形码还货"}
      },

      /** 移库任务 */
      {
        path: "moveTaskMenu",
        name: "moveTaskMenu",
        component: () => import("@/views/biz/moveBound/moveTask/index"),
        meta: {title: "移库"}
      },
      {
        path: "moveTask",
        name: "moveTask",
        component: () => import("@/views/biz/moveBound/moveTask/moveTask"),
        meta: {title: "领取移库任务"}
      },
      {
        path: "moveTaskTakeout",
        name: "moveTaskTakeout",
        component: () => import("@/views/biz/moveBound/moveTask/moveTaskTakeout"),
        meta: {title: "移库下架"}
      },
      {
        path: "moveTaskBoxPutaway",
        name: "moveTaskBoxPutaway",
        component: () => import("@/views/biz/moveBound/moveTask/moveTaskBoxPutaway"),
        meta: {title: "移库整箱上架"}
      },
      {
        path: "moveTaskItemPutaway",
        name: "moveTaskItemPutaway",
        component: () => import("@/views/biz/moveBound/moveTask/moveTaskItemPutaway"),
        meta: {title: "移库按件上架"}
      }
    ]
  }
];

export default moveBound;
