/*
 * @Author: xfworld
 * @Date: 2020-03-13 15:56:17
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-05 17:53:11
 * @Description:
 */
import Vue from "vue";
import VueRouter from "vue-router";
import routers from "./router";
import beforeEach from "./beforeEach";
//import afterEach from "./afterEach";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: routers,
  mode: "hash"
});

router.beforeEach(beforeEach);
//router.afterEach(afterEach);

export default router;
