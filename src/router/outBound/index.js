/*
 * @Author: xfworld
 * @Date: 2020-06-11 11:31:36
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 11:34:09
 * @Description:
 */
const outBound = [
  {
    path: "/outBound",
    name: "outBound",
    component: () => import("@/components/main/homeView"),
    children: [
      {
        path: "index",
        name: "outBoundIndex",
        component: () => import("@/views/biz/outBound/index"),
        meta: {
          title: "出库"
        }
      },
      {
        path: "pick",
        name: "pickIndex",
        component: () => import("@/views/biz/outBound/pick/index"),
        meta: {
          title: "拣货"
        }
      },
      {
        path: "b2cPickTask",
        name: "b2cPickTask",
        component: () => import("@/views/biz/outBound/pick/b2c/task"),
        meta: {title: "任务领取"}
      },
      {
        path: "b2cPickScanHeader",
        name: "b2cPickickScanHeader",
        component: () => import("@/views/biz/outBound/pick/b2c/pickScanHeader"),
        meta: {title: "拣货扫描"}
      },
      {
        path: "b2cPickickScanDetail",
        name: "b2cPickickScanDetail",
        component: () => import("@/views/biz/outBound/pick/b2c/pickScanDetail"),
        meta: {title: "拣货扫描详情"}
      },
      {
        path: "b2cPickScanTaskDetail",
        name: "b2cPickScanTaskDetail",
        component: () => import("@/views/biz/outBound/pick/b2c/pickScanTaskDetail"),
        meta: {title: "任务详情"}
      },
      {
        path: "sort",
        name: "sort",
        component: () => import("@/views/biz/outBound/sort"),
        meta: {
          title: "分货"
        }
      },
      {
        path: "sortPrint",
        name: "sortPrint",
        component: () => import("@/views/biz/outBound/sort/print"),
        meta: {
          title: "分货打印"
        }
      },
      {
        path: "lookAll",
        name: "lookAll",
        component: () => import("@/views/biz/outBound/sort/lookAll"),
        meta: {
          title: "查看全部"
        }
      },
      {
        path: "lookDiff",
        name: "lookDiff",
        component: () => import("@/views/biz/outBound/sort/lookDiff"),
        meta: {
          title: "查看差异"
        }
      },
      {
        path: "handOver",
        name: "handOverIndex",
        component: () => import("@/views/biz/outBound/handOver/index"),
        meta: {
          title: "交接"
        }
      },
      {
        path: "b2cHandOver",
        name: "b2cHandOver",
        component: () => import("@/views/biz/outBound/handOver/b2c"),
        meta: {
          title: "B2C交接"
        }
      },
      {
        path: "b2bHandOver",
        name: "b2bHandOver",
        component: () => import("@/views/biz/outBound/handOver/b2b"),
        meta: {
          title: "B2B交接"
        }
      },
      {
        path: "lookDiffCartonDetails",
        name: "lookDiffCartonDetails",
        component: () => import("@/views/biz/outBound/handOver/b2b/lookDiffCartonDetails"),
        meta: {
          title: "差异箱详情"
        }
      }
    ]
  }
];

export default outBound;
