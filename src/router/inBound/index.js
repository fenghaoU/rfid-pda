/*
 * @Author: xfworld
 * @Date: 2020-06-11 11:31:36
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 11:34:09
 * @Description:
 */
const inBound = [
  {
    path: "/inBound",
    name: "inBound",
    component: () => import("@/components/main/homeView"),
    children: [
      {
        path: "index",
        name: "inBoundIndex",
        component: () => import("@/views/biz/inBound/index")
      },
      {
        path: "putaway",
        name: "putawayIndex",
        component: () => import("@/views/biz/inBound/putaway/index")
      },
      {
        path: "b2bBoxPutaway",
        name: "b2bBoxPutaway",
        component: () => import("@/views/biz/inBound/putaway/b2b/boxPutaway"),
        meta: {title: "整箱上架"}
      },
      {
        path: "b2bItemPutaway",
        name: "b2bItemPutaway",
        component: () => import("@/views/biz/inBound/putaway/b2b/itemPutaway"),
        meta: {title: "按件上架"}
      },
      {
        path: "b2bItemQuery",
        name: "b2bItemQuery",
        component: () => import("@/views/biz/inBound/putaway/b2b/itemQuery"),
        meta: {title: "按件查询"}
      }
    ]
  }
];

export default inBound;
