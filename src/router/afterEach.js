/*
 * @Author: xfworld
 * @Date: 2020-06-03 14:34:43
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-10 15:18:43
 * @Description:
 */

//import { setTitle } from "@/libs/util";
// import store from "@/store";

export default function(to) {
  // store.dispatch("app/setLoadingStatus", false);
  const title = to.meta && to.meta.title;
  if (title) {
    document.title = title;
  }
}
