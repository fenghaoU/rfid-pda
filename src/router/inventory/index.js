/*
 * @Author: xfworld
 * @Date: 2020-06-11 11:31:36
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 11:34:09
 * @Description:
 */
const inventory = [
  {
    path: "/inventory",
    name: "inventory",
    component: () => import("@/components/main/homeView"),
    children: []
  }
];

export default inventory;
