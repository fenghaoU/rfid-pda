/*
 * @Author: xfworld
 * @Date: 2020-06-03 14:31:49
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-11 12:05:45
 * @Description:
 */

import inBound from "./inBound";
import outBound from "./outBound";
import moveBound from "./moveBound";
import inventory from "./inventory";

const routes = [
  ...inBound,
  ...outBound,
  ...moveBound,
  ...inventory,
  {
    path: "/home",
    name: "home",
    component: () => import("@/components/main/homeView"),
    meta: {
      title: "首页"
    },
    children: [
      {
        path: "storerWareBrand",
        name: "storerWareBrandSelect",
        component: () => import("@/views/home/index/storerWareBrandSelect")
      },
      {
        path: "index",
        name: "homeIndex",
        component: () => import("@/views/home/index")
      },
      {
        path: "person",
        name: "homePerson",
        component: () => import("@/views/home/person"),
        meta: {
          title: "个人中心"
        }
      },
      {
        path: "rfidScan/index",
        name: "rfidScan",
        component: () => import("@/views/rfidScan/index"),
        meta: {
          title: "RFID扫描"
        }
      },
      {
        path: "scanSettings/index",
        name: "scanSettings",
        component: () => import("@/views/scanSettings/index"),
        meta: {
          title: "RFID设置"
        }
      },
    ]
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/system/login"),
    meta: {
      title: "用户登录"
    }
  },
  {
    path: "/",
    redirect: "/home/index"
  },
  {
    path: "/404",
    name: "NotFound",
    component: () => import("@/components/errorPage/404.vue"),
    meta: {
      title: "页面丢失"
    }
  },
  {
    path: "/401",
    name: "AccessDenied",
    component: () => import("@/components/errorPage/401.vue"),
    meta: {
      title: "未经授权"
    }
  },
  {
    path: "/500",
    name: "InternalError",
    component: () => import("@/components/errorPage/500.vue"),
    meta: {
      title: "网站出错了"
    }
  },
  {
    path: "*",
    redirect: "/404"
  }
];

export default routes;
