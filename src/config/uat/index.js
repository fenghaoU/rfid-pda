/*
 * @Author: xfworld
 * @Date: 2020-05-12 15:21:51
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-02 10:04:50
 * @Description:
 */
const uat = {
  /**
   * @description: 整体的域名配置
   * @param {String}
   */
  appNameSpace: "pda_uat_",
  /**
   * @description api请求基础路径
   */
  //baseApiUrl: "http://10.128.32.141:10012/kwms365LiteApplication/",
  baseApiUrl: "http://10.128.32.141:10015/",
  /**
   * @description 描述优衣库项目北京仓库分货打印机云打印用户名
   */

  scanner: {use: true, type: "honeywell"}
};

export default uat;
