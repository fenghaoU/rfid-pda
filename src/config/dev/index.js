/*
 * @Author: xfworld
 * @Date: 2020-05-12 14:47:53
 * @LastEditors: xfworld
 * @LastEditTime: 2020-07-13 16:31:44
 * @Description:
 */
const dev = {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: "PDA",

  /**
   * @description: 整体的域名配置
   * @param {String}
   */
  appNameSpace: "pda_dev_",
  /**
   * @description token在Cookie中存储的天数，默认1天
   */
  cookieExpires: 1,
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: false,
  /**
   * @description api请求基础路径
   */
  baseApiUrl: "http://127.0.0.1:8081/kerryWmsOneIntegration/",
  /**
   * @description 默认打开的首页的路由name值，默认为home
   */
  homeName: "homeIndex",
  /**
   * @description 描述pda 专用扫描器类型 目前支持 zebra 和 honeywell
   */
  scanner: {use: false, type: "zebra_rfid"}
};

export default dev;
