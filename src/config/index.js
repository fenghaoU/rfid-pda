/*
 * @Author: xfworld
 * @Date: 2020-06-08 09:02:56
 * @LastEditors: xfworld
 * @LastEditTime: 2020-07-13 16:29:21
 * @Description:
 */
import baseConfig from "./dev";
import uatConfig from "./uat";
import sitConfig from "./sit";
import prodConfig from "./production";

const currentEnv = process.env.VUE_APP_CONTEXT;

// 当前模式下，对应存在的变量
let mergeConfig = {};
if (currentEnv === "uat") {
  Object.assign(mergeConfig, baseConfig, uatConfig);
} else if (currentEnv === "sit") {
  Object.assign(mergeConfig, baseConfig, sitConfig);
} else if (currentEnv === "production") {
  Object.assign(mergeConfig, baseConfig, prodConfig);
} else {
  Object.assign(mergeConfig, baseConfig);
}

const finalConfig = mergeConfig;

export default finalConfig;
