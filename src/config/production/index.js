/*
 * @Author: xfworld
 * @Date: 2020-05-12 15:22:39
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-13 09:33:26
 * @Description:
 */
const prod = {
  /**
   * @description: 整体的域名配置
   * @param {String}
   */
  appNameSpace: "pda_prod_",
  /**
   * @description api请求基础路径
   */
  baseApiUrl: "http://10.128.30.62/gatewayService/wms365/api/",
  /**
   * @description 描述pda 专用扫描器类型 目前支持 zebra 和 honeywell
   */
  scanner: {use: true, type: "honeywell"}
};

export default prod;
