/*
 * @Author: xfworld
 * @Date: 2020-05-26 10:53:27
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-08 09:06:12
 * @Description:
 */

import HttpRequest from "./fetch";
import config from "@/config";

const baseUrl = config.baseApiUrl;
const axios = new HttpRequest(baseUrl);
export default axios;
