import axios from "@libs/apiRequest";

const commonPath = "/api/handOverDeliveryB2B";

//B2B交接-装车号下拉框数据查询方法
export const getLoadingCartonNoList = (storerId, warehouseId, brandId) => {
    return axios.request({
        method: "get",
        url: commonPath.concat("/getLoadingCartonNoList"),
        params: {
            storerId: storerId,
            warehouseId: warehouseId,
            brandId: brandId
        }
    });
};

//B2B交接-在每次选择了装车号后获取已交接箱数和总箱数数量方法
export const getcartonSumFunction = (entruckingId, entruckingCode) => {
    return axios.request({
        method: "get",
        url: commonPath.concat("/getcartonSumFunction"),
        params: {
            entruckingId: entruckingId,
            entruckingCode: entruckingCode
        }
    });
};

//B2B交接-输入箱号时的执行的方法
export const getCartonNoDataFunction = (storerId, warehouseId, brandId, entruckingCode, entruckingName, cartonNo, bizUserName) => {
    return axios.request({
        method: "get",
        url: commonPath.concat("/getCartonNoDataFunction"),
        params: {
            storerId: storerId,
            warehouseId: warehouseId,
            brandId: brandId,
            entruckingId: entruckingCode,
            entruckingCode: entruckingName,
            cartonNo: cartonNo,
            userName: bizUserName
        }
    });
};
