import axios from "@libs/apiRequest";

const commonPath = "/api/handOver";

//按单交接后台-输入快递单号时的查询
export const getHandOverPdaData = (businessTypeCode, logisticsCode, deliveryKey, waybillNo, user) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/findHandOverWaybillNoFormData"),
    params: {
      businessType: businessTypeCode,
      logisticCompany: logisticsCode,
      deliveryKey: deliveryKey,
      waybillNo: waybillNo,
      userName: user
    }
  });
};
//勾选负数时扫描快递单号的操作
/*export const delHandOverWaybillNoFormData = (waybillNo) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat('/delHandOverWaybillNoFormData'),
        params: {
            'waybillNo': waybillNo
        }
    })
}*/
//按单交接-刷新合计按钮操作
/*export const refreshCombinedFunction = (deliveryKey) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat('/refreshCombinedFunction'),
        params: {
            'deliveryKey': deliveryKey
        }
    })
}*/
//按单交接-交接完成按钮操作
/*export const handoverCompleteFunction = (headerId) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat('/handoverCompleteFunction'),
        params: {
            'headerId': headerId
        }
    })
}*/
//按单交接-交接重开按钮操作
/*export const handoverRestartFunction = (deliveryKey) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat('/handoverRestartFunction'),
        params: {
            'deliveryKey': deliveryKey
        }
    })
}*/

export const getSuccessCartonNum = deliveryKey => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getSuccessCartonNum"),
    params: {
      deliveryKey: deliveryKey
    }
  });
};
