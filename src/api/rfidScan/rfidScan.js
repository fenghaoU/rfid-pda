import axios from "@libs/apiRequest";

const commonPath = "/api/uggRfidRecord";

// 保存RFID扫描数据
export const saveRfidScanData = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/saveRfidScanData"),
    data: data
  });
};
