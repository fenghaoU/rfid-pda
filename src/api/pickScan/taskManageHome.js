import axios from "@libs/apiRequest";

//查询任务列表
export const selectTaskListApi = (storerId, warehouseId, brandId, businessType, userCode, queryType) => {
  return axios.request({
    method: "get",
    url:
      "/api/bizPickPicker/getUserPickTask" +
      "/" +
      storerId +
      "/" +
      warehouseId +
      "/" +
      brandId +
      "/" +
      businessType +
      "/" +
      userCode +
      "/" +
      queryType
  });
};

//领取任务
export const getTaskApi = (storerId, warehouseId, brandId, businessType, userCode) => {
  return axios.request({
    method: "get",
    url: "/api/pickTaskMng/receivePickTask/" + storerId + "/" + warehouseId + "/" + brandId + "/" + businessType + "/" + userCode
  });
};

// 领取任务 -- 输入任务号进行领取任务
export const getTaskWithTaskCode = (storerId, warehouseId, brandId, businessType, pickTaskCode, userCode) => {
  return axios.request({
    method: "get",
    url:
      "/api/pickTaskMng/receivePickTaskWithTaskCode/" +
      storerId +
      "/" +
      warehouseId +
      "/" +
      brandId +
      "/" +
      businessType +
      "/" +
      pickTaskCode +
      "/" +
      userCode
  });
};
