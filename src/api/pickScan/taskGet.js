import axios from "@libs/apiRequest";

const commonPath = "/api/taskGet";
export const findDataFunction = (storerCode, wareHouseCode, brandCode, aisle) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/findDataFunction"),
    params: {
      storerCode: storerCode,
      wareHouseCode: wareHouseCode,
      brandCode: brandCode,
      aisle: aisle
    }
  });
};

export const updateDataFunction = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/updateDataFunction"),
    data: data
  });
};
