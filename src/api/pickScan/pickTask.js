import axios from "@libs/apiRequest";

const commonPath = "/api/pickTaskMng";

//  库位信息
export const getLocationNoData = (pickTaskId, userCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getPickTaskLocation/" + pickTaskId + "/" + userCode)
  });
};

//   sku信息
export const getSkuData = (pickTaskId, locationId, userCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getPIckTaskLocationSku/" + pickTaskId + "/" + locationId + "/" + userCode)
  });
};

//  扫描记录日记
export const insertPickingLog = requestData => {
  return axios.request({
    method: "post",
    url: "/api/bizPickOpeationLog/insertPickingLog",
    data: requestData
  });
};

//  负数记录日记
export const deletePickLog = requestData => {
  return axios.request({
    method: "post",
    url: "/api/bizPickOpeationLog/deletePickLog",
    data: requestData
  });
};

//  获取扫描明细，取三条
export const getPickLogByRefresh = (storerId, warehouseId, brandId, picktaskId, cartonId) => {
  return axios.request({
    method: "get",
    url: "/api/bizPickOpeationLog/getPickLogByRefresh/" + storerId + "/" + warehouseId + "/" + brandId + "/" + picktaskId + "/" + cartonId
  });
};

//  获取扫描数量
export const computePickedQtySum = (storerId, warehouseId, brandId, pickTaskIdString, locationId, sku) => {
  return axios.request({
    method: "get",
    url:
      "/api/bizPickOpeationLog/computePickedQtySum/" +
      storerId +
      "/" +
      warehouseId +
      "/" +
      brandId +
      "/" +
      pickTaskIdString +
      "/" +
      locationId +
      "/" +
      sku
  });
};

//  任务关闭
export const closePickTask = (storerId, warehouseId, brandId, pickTaskId, userCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/closeTask/" + storerId + "/" + warehouseId + "/" + brandId + "/" + pickTaskId + "/" + userCode)
  });
};

/**
 * 格式化时间戳
 * @param inDate number
 * @returns (string)年-月-日 时:分:秒
 */
export const formatNumberToDateTime = inDate => {
  if (inDate) {
    let date = new Date(Number(inDate));
    const Y = date.getFullYear();
    let M = date.getMonth() + 1;
    M = M < 10 ? "0" + M : M;
    let D = date.getDate();
    D = D < 10 ? "0" + D : D;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let m = date.getMinutes();
    m = m < 10 ? "0" + m : m;
    let s = date.getSeconds();
    s = s < 10 ? "0" + s : s;
    return Y + "-" + M + "-" + D + " " + h + ":" + m + ":" + s;
  }
  return "";
};

/*export const getLocationNoData = (pickTaskId,userCode) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat("/getPickTaskLocation/" + templateName + "/" + projectNo ),
    });
};

export const scanBarCode = (pickTaskCode,brandCode,aisleCode,skuCarton,barCode,qty) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat("/scanBarCode"),
        params:{
            pickTaskCode:pickTaskCode,
            brandCode:brandCode,
            aisleCode:aisleCode,
            skuCarton:skuCarton,
            barCode:barCode,
            qty:qty
        }
    });
};


export const scandetail = (relId,aisle) => {
    return axios.request({
        method: 'get',
        url: commonPath.concat("/scanDetail"),
        params:{
            relId:relId,
            aisle:aisle
        }
    });
};
*/
// 出库-拣货-扫描-任务号
export const receivePickTaskWithTaskCode = params => {
  return axios.request({
    method: "get",
    url: "/api/pickTaskMng/receivePickTaskWithTaskCode",
    params: params
  });
};

// 出库-拣货-详情查看
export const getAllPickLogByTaskId = params => {
  return axios.request({
    method: "get",
    url: "/api/bizPickOpeationLog/getAllPickLogByTaskId",
    params: params
  });
};
