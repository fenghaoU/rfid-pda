import axios from "@libs/apiRequest";

//查询拣货任务详情
export const selectTaskManagerDetailApi = headerId => {
  return axios.request({
    method: "get",
    url: "/api/bizPicktaskDetail/findPickTaskDetailData/" + headerId
  });
};

//任务重开
export const reOpenTaskApi = (storerId, warehouseId, brandId, pickTaskId, userCode) => {
  return axios.request({
    method: "get",
    url: "/api/pickTaskMng/openTask/" + storerId + "/" + warehouseId + "/" + brandId + "/" + pickTaskId + "/" + userCode
  });
};

//任务关闭
export const closeTaskApi = (storerId, warehouseId, brandId, pickTaskId, userCode) => {
  return axios.request({
    method: "get",
    url: "/api/pickTaskMng/closeTask/" + storerId + "/" + warehouseId + "/" + brandId + "/" + pickTaskId + "/" + userCode
  });
};

export const getCartonList = (storerId, warehouseId, brandId, picktaskId, userCode) => {
  return axios.request({
    method: "get",
    url: "/api/bizPickCartonNo/getCartonList/" + storerId + "/" + warehouseId + "/" + brandId + "/" + picktaskId + "/" + userCode
  });
};

export const getPickLogByCarton = (storerId, warehouseId, brandId, picktaskId, cartonId) => {
  return axios.request({
    method: "get",
    url: "/api/bizPickOpeationLog/getPickLogByCarton/" + storerId + "/" + warehouseId + "/" + brandId + "/" + picktaskId + "/" + cartonId
  });
};
