import axios from "@libs/apiRequest";

const commonPath = "/common";

export const getDictData = dictGroup => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getDict/", dictGroup)
  });
};

export const getStoreData = (type, userId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getStoreData/", type + "/" + userId)
  });
};

export const getWareHouseData = storerId => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getWareHouse/", storerId)
  });
};

export const getBrandData = (dictGroup, storerId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getBrandData/", dictGroup + "/" + storerId)
  });
};

export const getAllWareHouseData = () => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getAllWareHouse")
  });
};

export const getCartonData = (type, storerId) => {
  return axios.request({
    method: "get",
    url: "api/cartonStanderMasterManager/getCartonData/" + type + "/" + storerId
  });
};
export const getOptionData = (dictGroup, storerId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getOptionData/", dictGroup + "/" + storerId)
  });
};

export const addPreZero = num => {
  return ("000000000" + num).slice(-10);
};

export const getWarehouseData = () => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getWarehouseData")
  });
};
