/**
 * 格式化日期
 * 传入datetime格式的字符串
 */
export const substringDateTimeToDate = input => {
  if (input !== "" && input != undefined && input != null) {
    let res = input.substring(0, 10);
    return res;
  } else {
    return "error";
  }
};
/**
 * 日期选择框
 * @param inDate
 * @returns 年-月-日 时:分:秒
 */
export const formatDateToDateTime = inDate => {
  if (inDate) {
    let date = new Date(inDate);
    const Y = date.getFullYear();
    let M = date.getMonth() + 1;
    M = M < 10 ? "0" + M : M;
    let D = date.getDate();
    D = D < 10 ? "0" + D : D;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let m = date.getMinutes();
    m = m < 10 ? "0" + m : m;
    let s = date.getSeconds();
    s = s < 10 ? "0" + s : s;
    return Y + "-" + M + "-" + D + " " + h + ":" + m + ":" + s;
  }
  return "";
};
/**
 * 格式化时间戳
 * @param inDate number
 * @returns (string)年-月-日 时:分:秒
 */
export const formatNumberToDateTime = inDate => {
  if (inDate) {
    let date = new Date(Number(inDate));
    const Y = date.getFullYear();
    let M = date.getMonth() + 1;
    M = M < 10 ? "0" + M : M;
    let D = date.getDate();
    D = D < 10 ? "0" + D : D;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let m = date.getMinutes();
    m = m < 10 ? "0" + m : m;
    let s = date.getSeconds();
    s = s < 10 ? "0" + s : s;
    return Y + "-" + M + "-" + D + " " + h + ":" + m + ":" + s;
  }
  return "";
};
/**
 * 格式化日期
 * @param inDate
 * @returns 年-月-日
 */
export const formatDateToDate = inDate => {
  if (inDate) {
    let date = new Date(Number(inDate));
    const Y = date.getFullYear();
    let M = date.getMonth() + 1;
    M = M < 10 ? "0" + M : M;
    let D = date.getDate();
    D = D < 10 ? "0" + D : D;
    return Y + "-" + M + "-" + D;
  }
  return "";
};
/**
 * 格式化日期
 * @param inDate
 * @returns 年-月-日
 */
export const formatNumberToDate = inDate => {
  if (inDate) {
    let date = new Date(Number(inDate));
    const Y = date.getFullYear();
    let M = date.getMonth() + 1;
    M = M < 10 ? "0" + M : M;
    let D = date.getDate();
    D = D < 10 ? "0" + D : D;
    return Y + "-" + M + "-" + D;
  }
  return "";
};
/**
 * 格式化当前时间
 * @return {*}yyyyMMddHHmmSS
 */
export const formatNowTime = () => {
  let date = new Date();
  const Y = date.getFullYear();
  let M = date.getMonth() + 1;
  M = M < 10 ? "0" + M : M;
  let D = date.getDate();
  D = D < 10 ? "0" + D : D;
  let h = date.getHours();
  h = h < 10 ? "0" + h : h;
  let m = date.getMinutes();
  m = m < 10 ? "0" + m : m;
  let s = date.getSeconds();
  s = s < 10 ? "0" + s : s;
  return Y + M + D + h + m + s;
};
