import axios from "@libs/apiRequest";

const commonPath = "/api/selectOption";

export const getStorerSelect = userId => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getStorerSelect/", userId)
  });
};

export const getWarehouseSelect = (storerId, userId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getWarehouseSelect/", storerId, "/", userId)
  });
};

export const getBrandSelect = (storerId, warehouseId, userId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getBrandSelect/", storerId, "/", warehouseId, "/", userId)
  });
};

export const getDictSelect = (storerId, warehouseId, brandId, userId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getDictSelect/", storerId, "/", warehouseId, "/", brandId, "/", userId)
  });
};

export const getDictChange = (grantKey, transformType, grantValue) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getDictChange/", grantKey, "/", transformType, "/", grantValue)
  });
};
