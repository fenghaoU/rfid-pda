import axios from "@libs/apiRequest";

const commonPath = "/api/pdaPrinterConfig";

export const getPrinterTempInfo = printerName => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getPrinterTempInfo"),
    params: {
      printerName: printerName
    }
  });
};
