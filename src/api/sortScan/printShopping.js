import axios from "@libs/apiRequest";

const commonPath = "/api/bizRelReceiver";

export const printShoppingListWithBatch = (pickTaskCode, printerName, clientId, storerCode, templateName) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/printShoppingListWithBatch"),
    params: {
      pickTaskCode: pickTaskCode,
      printerName: printerName,
      clientId: clientId,
      storerCode: storerCode,
      templateName: templateName
    }
  });
};
