import axios from "@libs/apiRequest";

const commonPath = "/api/sortHeader";

export const getPickTaskPdaData = (storerId, warehouseId, brandId, pickTaskCode, type, userName) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getPickTaskPdaData"),
    params: {
      storerId: storerId,
      warehouseId: warehouseId,
      brandId: brandId,
      pickTaskCode: pickTaskCode,
      type: type,
      userName: userName
    }
  });
};

export const updatePdaCarNo = (pickTaskCode, carNo) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/updatePdaCarNo"),
    params: {
      pickTaskCode: pickTaskCode,
      carNo: carNo
    }
  });
};

export const getCarLineNo = (storerId, warehouseId, brandId, pickTaskCode, barCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getCarLineNo"),
    params: {
      storerId: storerId,
      warehouseId: warehouseId,
      brandId: brandId,
      pickTaskCode: pickTaskCode,
      barCode: barCode
    }
  });
};

export const computeBarCodeSum = (storerId, warehouseId, brandId, pickTaskCode, barCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/computeBarCodeSum"),
    params: {
      storerId: storerId,
      warehouseId: warehouseId,
      brandId: brandId,
      pickTaskCode: pickTaskCode,
      barCode: barCode
    }
  });
};

export const lookPdaAllOrDiffData = (pickTaskCode, type) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/lookPdaAllOrDiffData"),
    params: {
      pickTaskCode: pickTaskCode,
      type: type
    }
  });
};

export const printPross = (pickTaskCode, userName) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/printPross"),
    params: {
      pickTaskCode: pickTaskCode,
      userName: userName
    }
  });
};

// 出库-B2C分货-扫描-箱号
export const getSortDetailWithPda = params => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getSortDetailWithPda"),
    params: params
  });
};

// 出库-B2C分货-扫描-条形码
export const getCarLineNoWithPda = params => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getCarLineNoWithPda"),
    params: params
  });
};

// 出库-B2C分货-扫描-格子条码
export const saveSortCartonCodePda = params => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/saveSortCartonCodePda"),
    params: params
  });
};

// 出库-B2C分货-查询全部/差异
export const lookAllOrDiffDataPda = params => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/lookAllOrDiffDataPda"),
    params: params
  });
};
