import axios from "@libs/apiRequest";

const commonPath = "/api/expressControl";

export const expressBatchPrint = (pickTaskCode, printerName, templateName, clientId, storerCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/expressBatchPrint"),
    params: {
      pickTaskCode: pickTaskCode,
      printerName: printerName,
      templateName: templateName,
      clientId: clientId,
      storerCode: storerCode
    }
  });
};

export const getPrinterName = (expressTemplateName, shoppingTemplateName, userId) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getPrinterName"),
    params: {
      expressTemplateName: expressTemplateName,
      shoppingTemplateName: shoppingTemplateName,
      userId: userId
    }
  });
};
