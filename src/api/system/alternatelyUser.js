/*
 * @Author: xfworld
 * @Date: 2020-06-05 16:07:52
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-05 17:23:59
 * @Description:
 */

import axios from "@libs/apiRequest";

const apiPath = {
  commonPath: "/api/userView",
  getList: "/getList"
};

const apiPathResource = {
  commonPathResource: apiPath.commonPath,
  getListResource: apiPath.commonPath.concat(apiPath.getList)
};

export const getUserViewPage = (current, size, searchData) => {
  return axios.request({
    method: "get",
    url: apiPathResource.commonPathResource.concat("/page/", current, "/pageNum/", size),
    params: {
      loginName: searchData.loginName,
      orgCode: searchData.orgCode,
      bizUserName: searchData.bizUserName,
      bizUserAccount: searchData.bizUserAccount
    }
  });
};

export const getUserViewList = () => {
  return axios.request({
    method: "get",
    url: apiPathResource.getListResource
  });
};

export const saveUserView = user => {
  return axios.request({
    method: "post",
    url: apiPathResource.commonPathResource,
    data: user
  });
};
export const updateUserView = (userId, user) => {
  return axios.request({
    method: "put",
    url: apiPathResource.commonPathResource.concat("/", userId),
    data: user
  });
};
export const deleteUserView = userId => {
  return axios.request({
    method: "delete",
    url: apiPathResource.commonPathResource.concat("/", userId)
  });
};
export const getUserView = id => {
  return axios.request({
    method: "get",
    url: apiPathResource.commonPathResource.concat("/", id)
  });
};
