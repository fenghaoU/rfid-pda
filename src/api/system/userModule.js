/*
 * @Author: xfworld
 * @Date: 2019-08-09 09:45:14
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-02 10:02:41
 * @Description:
 */

import axios from "@libs/apiRequest";

const sysUserModuleApi = {
  commonPath: "/api/system/sysUserModule",
  getGrantModuleLableByUser: "/getGrantModuleLableByUser",
  getMobileGrantModuleLableByUser: "/getMobileGrantModuleLableByUser"
};

const sysUserModuleApiResource = {
  getGrantModuleLableByUserResource: sysUserModuleApi.commonPath.concat(sysUserModuleApi.getGrantModuleLableByUser),
  getMobileGrantModuleLableByUserResource: sysUserModuleApi.commonPath.concat(sysUserModuleApi.getMobileGrantModuleLableByUser)
};

export const getGrantModuleLableByUser = userId => {
  return axios.request({
    method: "get",
    url: sysUserModuleApiResource.getGrantModuleLableByUserResource.concat("/", userId)
  });
};

export const getMobileGrantModuleLableByUser = userId => {
  return axios.request({
    method: "get",
    url: sysUserModuleApiResource.getMobileGrantModuleLableByUserResource.concat("/", userId)
  });
};
