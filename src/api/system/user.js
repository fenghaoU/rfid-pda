/*
 * @Author: xfworld
 * @Date: 2020-05-28 14:41:51
 * @LastEditors: xfworld
 * @LastEditTime: 2020-05-28 14:42:51
 * @Description:
 */
import fetch from "@libs/apiRequest";

const sysUserAPI = {
  commonPath: "/api/system/sysUser",
  resetPassWord: "/resetPassWord",
  modifyUserPassWord: "/modifyUserPassWord"
};

const sysUserAPIResource = {
  commonPathResource: sysUserAPI.commonPath,
  resetPassWordResource: sysUserAPI.commonPath.concat(sysUserAPI.resetPassWord),
  modifyUserPassWordResource: sysUserAPI.commonPath.concat(sysUserAPI.modifyUserPassWord)
};

export const resetPassWord = userId => {
  return fetch.request({
    method: "get",
    url: sysUserAPIResource.resetPassWordResource.concat("/", userId)
  });
};

export const modifyUserPassWord = (userId, passWord) => {
  return fetch.request({
    method: "put",
    url: sysUserAPIResource.modifyUserPassWordResource.concat("/", userId),
    data: passWord
  });
};
