/*
 * @Author: xfworld
 * @Date: 2020-06-05 16:08:55
 * @LastEditors: xfworld
 * @LastEditTime: 2020-06-05 16:09:25
 * @Description:
 */

import axios from "@libs/apiRequest";

const sysUserRoleApi = {
  commonPath: "/api/system/sysUserRole",
  userAuthRoleList: "/userAuthRoleList",
  queryUserRoleList: "/queryUserRoleList",
  queryGrantRoleData: "/queryGrantRoleData",
  queryAdminRole: "/queryAdminRole"
};

const sysUserRoleApiResource = {
  commonPathResource: sysUserRoleApi.commonPath,
  userAuthRoleListResource: sysUserRoleApi.commonPath.concat(sysUserRoleApi.userAuthRoleList),
  queryUserRoleListResource: sysUserRoleApi.commonPath.concat(sysUserRoleApi.queryUserRoleList),
  queryGrantRoleDataResource: sysUserRoleApi.commonPath.concat(sysUserRoleApi.queryGrantRoleData),
  queryAdminRoleResource: sysUserRoleApi.commonPath.concat(sysUserRoleApi.queryAdminRole)
};

export const userAuthRoleList = (userId, roleList) => {
  return axios.request({
    method: "put",
    url: sysUserRoleApiResource.userAuthRoleListResource.concat("/", userId),
    data: roleList
  });
};
export const queryUserRoleList = userId => {
  return axios.request({
    method: "get",
    url: sysUserRoleApiResource.queryUserRoleListResource.concat("/", userId)
  });
};
export const queryGrantRoleData = () => {
  return axios.request({
    method: "get",
    url: sysUserRoleApiResource.queryGrantRoleDataResource
  });
};

export const queryAdminRole = userId => {
  return axios.request({
    method: "get",
    url: sysUserRoleApiResource.queryAdminRoleResource.concat("/", userId)
  });
};
