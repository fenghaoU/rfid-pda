import axios from "@libs/apiRequest";

const commonPath = "/api/returnWarehouse";

export const getSkuPdaData = sku => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getSkuPdaData"),
    params: {
      sku: sku
    }
  });
};

export const saveLocationPdaData = (sku, location) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/saveLocationPdaData"),
    params: {
      sku: sku,
      location: location
    }
  });
};
