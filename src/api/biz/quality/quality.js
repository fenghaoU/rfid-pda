import axios from "@libs/apiRequest";

// 质检下架
// 库位
export const validateLocationCode = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/takeout/validateLocationCode",
    data: data
  });
};

// 箱号
export const doTakeout = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/takeout/doTakeout",
    data: data
  });
};

// 质检分货

// 库位属性
export const getQALocations = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/sort/getQALocations",
    data: data
  });
};

// 原始箱号
export const getSourceCarton = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/sort/getSourceCarton",
    data: data
  });
};

// 目标箱号
export const scanTargetCarton = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/sort/scanTargetCarton",
    data: data
  });
};

// SKU
export const scanSku = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/sort/scanSku",
    data: data
  });
};

// 关箱
export const closeTargetCarton = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/sort/closeTargetCarton",
    data: data
  });
};

// 质检上架

// 库位
export const scanLocationCode = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/putaway/scanLocationCode",
    data: data
  });
};

// 箱号
export const scanSourceCarton = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/putaway/scanSourceCarton",
    data: data
  });
};

// 质检上架 - 按件

// 质检-按件上架-扫描库位
export const scanPieceLocation = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/piece/scanPieceLocation",
    data: data
  });
};

// 质检-按件上架-扫描箱号
export const scanBoxNumber = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/piece/scanBoxNumber",
    data: data
  });
};

// 质检-按件上架-扫描sku
export const scanPieceSku = data => {
  return axios.request({
    method: "post",
    url: "/api/bizMoveQuality/piece/scanPieceSku",
    data: data
  });
};
