import axios from "@libs/apiRequest";

const commonPath = "/api/pdaMoveMgmt";

// 验证移库单
export const verifyMoveCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyMoveCode"),
    data: data
  });
};

// 验证原始库位
export const verifyFromLocationCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyFromLocationCode"),
    data: data
  });
};

// 验证移库箱号
export const verifyMoveCartonNumber = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyMoveCartonNumber"),
    data: data
  });
};
verifyBarCode;

// 验证barCode
export const verifyBarCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyBarCode"),
    data: data
  });
};

// 执行移库下架
export const executeMoveTakeout = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/executeMoveTakeout"),
    data: data
  });
};

/** 移库上架 */
// 验证移库箱号
export const verifyMoveCartonNumberForPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyMoveCartonNumberForPutaway"),
    data: data
  });
};

// 验证库位
export const verifyLocationCodeForPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyLocationCodeForPutaway"),
    data: data
  });
};

// 执行移库上架
export const executeMoveBoxPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/executeMoveBoxPutaway"),
    data: data
  });
};

// 验证barCode
export const verifyBarCodeForPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyBarCodeForPutaway"),
    data: data
  });
};

// 按件上架
export const executeMoveItemPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/executeMoveItemPutaway"),
    data: data
  });
};
