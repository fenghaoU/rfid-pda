import axios from "@libs/apiRequest";

const commonPath = "/api/pdaMoveMgmt";

// 根据用户查询移库任务
export const getMoveTaskByUser = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/getMoveTaskByUser"),
    data: data
  });
};

// 领取移库任务
export const takeMoveTask = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/takeMoveTask"),
    data: data
  });
};

// 根据移库单号查询待移库下架数据
export const getWaitTakeOutDataByMoveCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/getWaitTakeOutDataByMoveCode"),
    data: data
  });
};

// 执行移库下架操作
export const doMoveTakeout = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/doMoveTakeout"),
    data: data
  });
};

// 移库上架，通过移库任务号获取待上架数据
export const getWaitMovePutawayData = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/getWaitMovePutawayData"),
    data: data
  });
};

// 执行移库按箱上架
export const doMoveBoxPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/doMoveBoxPutaway"),
    data: data
  });
};

// 执行移库按件上架
export const doMoveItemPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/doMoveItemPutaway"),
    data: data
  });
};
