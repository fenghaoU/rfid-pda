import axios from "@libs/apiRequest";

const commonPath = "/api/bizPutawayHeaderEntity";
const basePath = "/api/selectOption";

// 通过用户获取货主
export const getStorerSelect = userId => {
  return axios.request({
    method: "get",
    url: basePath.concat("/getStorerSelect/", userId)
  });
};

// 通过货主和用户获取仓库信息
export const getWarehouseSelect = (storerId, userId) => {
  return axios.request({
    method: "get",
    url: basePath.concat("/getWarehouseSelect/", storerId, "/", userId)
  });
};

// 通过货主仓库获取品牌信息
export const getBrandSelect = (storerId, warehouseId, userId) => {
  return axios.request({
    method: "get",
    url: basePath.concat("/getBrandSelect/", storerId, "/", warehouseId, "/", userId)
  });
};

// 验证箱号是否存在
export const verifyRecCartonNumber = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyRecCartonNumber"),
    data: data
  });
};

// 验证库位是否存在
export const verifyLocation = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifyLocation"),
    data: data
  });
};

// 验证SKU是否存在
export const verifySku = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/verifySku"),
    data: data
  });
};

// 执行整箱上架操作
export const excuteBoxPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/excuteBoxPutaway"),
    data: data
  });
};

// 执行按件上架操作
export const excuteItemPutaway = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/excuteItemPutaway"),
    data: data
  });
};

// 执行按单查询
export const excuteItemQuery = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/excuteItemQuery"),
    data: data
  });
};
