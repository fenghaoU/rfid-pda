import axios from "@libs/apiRequest";

const commonPath = "/api/returnInventoryScanMgmt";

// 根据商品二维码获取基础数据
export const verifyQrCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/getReturnInventoryScanData"),
    data: data
  });
};

// 执行还货扫描
export const executeReturnScan = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/executeReturnScan"),
    data: data
  });
};

// 根据商品条码获取基础数据
export const verifyBarCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/getReturnInventoryScanDataByBarCode"),
    data: data
  });
};

// 扫描商品条码，执行还货扫描
export const executeReturnScanByBarCode = data => {
  return axios.request({
    method: "post",
    url: commonPath.concat("/executeReturnScanByBarCode"),
    data: data
  });
};