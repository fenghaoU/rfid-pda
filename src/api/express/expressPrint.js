import axios from "@libs/apiRequest";

const commonPath = "/api/expressControl";

export const getOrderPrintConfig = orderId => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getOrderConfig/" + orderId)
  });
};

export const orderBillPrint = (orderId, customId, clientId, expressType, printerName, templateName, whCode) => {
  return axios.request({
    method: "get",
    url: commonPath.concat(
      "/expressPrint/" +
        orderId +
        "/" +
        customId +
        "/" +
        clientId +
        "/" +
        expressType +
        "/" +
        printerName +
        "/" +
        templateName +
        "/" +
        whCode
    )
  });
};
