import axios from "@libs/apiRequest";

export const getPrintConfig = (storerId, printType) => {
  return axios.request({
    method: "get",
    url: "/api/projectPrintAttr/getProjectPrintConfig/" + storerId + "/" + printType
  });
};
