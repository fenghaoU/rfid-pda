import axios from "@libs/apiRequest";

const commonPath = "/api/bizRelReceiver";

export const getReceiveInfor = orderId => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/getReceiverInfor/", orderId)
  });
};

export const printShoppingList = (orderId, cartonId, clientId, printerName, templateName) => {
  return axios.request({
    method: "get",
    url: commonPath.concat("/printShoppingList/", orderId, "/", cartonId, "/", clientId, "/", printerName, "/", templateName)
  });
};
