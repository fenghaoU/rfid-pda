/*
 * @Author: xfworld
 * @Date: 2020-05-28 14:25:14
 * @LastEditors: xfworld
 * @LastEditTime: 2020-05-28 14:29:13
 * @Description:
 */
import fetch from "@libs/apiRequest";

export const login = ({userName, password}) => {
  let params = {
    userName: userName,
    passWord: password
  };
  return fetch.request({
    method: "post",
    url: "login",
    data: params
  });
};

export const logout = token => {
  return fetch.request({
    url: "logout",
    method: "post",
    data: token
  });
};
